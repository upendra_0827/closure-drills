 
function cacheFunction(cb) {                         // declaring the function

    const cache = {}                                   // to store the already called input

    return function(val) {                           // function to return

        if (!(typeof(val)==='number')) {              // checking the input
            return 'Invalid input'
            
        } else {

        if (val in cache) {                           // checking the input is already passed
            return cache[val]
        } else {

            let ans = cb(val)                         // calling the calback function
            cache[val] = ans
            return `The function has been called, ${ans}`
        }
        }
    }

}

module.exports = cacheFunction                   // exporting the function

