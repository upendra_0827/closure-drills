function counterFactory(element) {                           // declaring the function
    let value = element

    if (!(typeof value === 'number') || value === 0) {       // checking the input type
         return false                                          // if the input is of undesired type
    } else {
        return object_function = {                           // returning object with the functions
            increment : function() {                       // increment function
                return value += 1
            },
            decrement : function() {                         // decrement function
                return value -= 1
            }
        }
    }
}

module.exports = counterFactory                             // exporting function