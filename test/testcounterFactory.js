
const function_check = require('../counterFactory.js')                    // importing the function

const value = function_check('hi')                                           // input a number

if (value === false) {
    console.log('Invalid input')
} else {
    console.log(value.increment())
}
