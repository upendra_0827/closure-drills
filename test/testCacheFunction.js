
let function_cache = require('../cacheFunction.js')                  // importing the function

const cb = num => num**2

let cal = function_cache(cb)

console.log(cal(2))                                                // input a number
console.log(cal(2))
console.log(cal(3))
console.log(cal(3))
console.log(cal(true))                                              // test case 2
console.log(cal('hi'))                                              // test case 3