
const limit = require('../limitFunctionCallCount.js')          // importing the function

const cb = number => number**4                                         // calback function

const function_check = limit(cb,3)                             // function to check the main function          

console.log(function_check(3))                                             // input a number
console.log(function_check(3))
console.log(function_check(3))
console.log(function_check(3))
console.log(function_check('hi'))                                // test case 2
console.log(function_check(true))                                // test case 3