
function limitFunctionCallCount(cb,number) {                         // declaring a function

    let count = 0                                                   // to count number of invokes

    return function invoke_function(value) {                         // returning function to invoke calback function
        if (!(typeof value === 'number')) {                        // checking the type of input
            return 'Invalid input'
        } else {
        if (count >= number) {                                // checking the number of invokes of calback function
            return 'null'
        } else {
            count += 1                                      // counting the number of calback function invokes
            return cb(value)
        }}
    }
}

module.exports = limitFunctionCallCount                           // exporting the function





